# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.1"></a>
## [1.1.1](https://bitbucket.org/millionspaces/ms-host-backend/compare/v1.1.0...v1.1.1) (2019-03-28)



<a name="1.1.0"></a>
# 1.1.0 (2019-03-28)


### Bug Fixes

* **package.json:** pretty config changed ([e85cc8e](https://bitbucket.org/millionspaces/ms-host-backend/commits/e85cc8e))


### Features

* **Create Space:** Create a space ([157d73e](https://bitbucket.org/millionspaces/ms-host-backend/commits/157d73e))
* **user service:** Get all Spaces of a user ([95471fe](https://bitbucket.org/millionspaces/ms-host-backend/commits/95471fe))


### Performance Improvements

* **tslint.json:** stop some rules ([7827f60](https://bitbucket.org/millionspaces/ms-host-backend/commits/7827f60))
* **tslint.json:** tslint renamed & rule changed ([85a099f](https://bitbucket.org/millionspaces/ms-host-backend/commits/85a099f))
