-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mssg
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mssg
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mssg` DEFAULT CHARACTER SET utf8 ;
USE `mssg` ;

-- -----------------------------------------------------
-- Table `mssg`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`user_role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mssg`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`address` (
  `id` INT NOT NULL,
  `line1` VARCHAR(45) NOT NULL,
  `line2` VARCHAR(45) NULL,
  `zipCode` VARCHAR(45) NULL,
  `city` VARCHAR(45) NOT NULL,
  `latitude` VARCHAR(10) NULL,
  `longitude` VARCHAR(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mssg`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NULL,
  `phoneNumber` VARCHAR(12) NOT NULL,
  `userRoleId` INT NOT NULL,
  `addressId` INT NOT NULL,
  `verified` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_user_user_role1_idx` (`userRoleId` ASC),
  INDEX `fk_user_address1_idx` (`addressId` ASC),
  CONSTRAINT `fk_user_user_role1`
    FOREIGN KEY (`userRoleId`)
    REFERENCES `mssg`.`user_role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_address1`
    FOREIGN KEY (`addressId`)
    REFERENCES `mssg`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mssg`.`organization`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`organization` (
  `id` INT NOT NULL,
  `organizationName` VARCHAR(45) NOT NULL,
  `phoneNumber` VARCHAR(12) NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mssg`.`commission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`commission` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `commission` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mssg`.`space`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mssg`.`space` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `spaceName` VARCHAR(45) NOT NULL,
  `description` VARCHAR(145) NULL,
  `organizationId` INT NOT NULL,
  `userId` INT NOT NULL,
  `addressId` INT NOT NULL,
  `commissionId` INT NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` TINYINT(1) NOT NULL DEFAULT 0,
  `delete` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_space_organization1_idx` (`organizationId` ASC),
  INDEX `fk_space_user1_idx` (`userId` ASC),
  INDEX `fk_space_address1_idx` (`addressId` ASC),
  INDEX `fk_space_commission1_idx` (`commissionId` ASC),
  CONSTRAINT `fk_space_organization1`
    FOREIGN KEY (`organizationId`)
    REFERENCES `mssg`.`organization` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_user1`
    FOREIGN KEY (`userId`)
    REFERENCES `mssg`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_address1`
    FOREIGN KEY (`addressId`)
    REFERENCES `mssg`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_commission1`
    FOREIGN KEY (`commissionId`)
    REFERENCES `mssg`.`commission` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;







INSERT INTO `mssg`.`address` (`id`, `line1`, `line2`, `zipCode`, `city`, `latitude`, `longitude`) VALUES ('1', 'Urumpirai North', 'Urumpirai', '40000', 'Jaffna', 'x', 'y');
INSERT INTO `mssg`.`user_role` (`id`, `name`, `active`) VALUES ('1', 'user', '1');
INSERT INTO `mssg`.`commission` (`id`, `commission`) VALUES ('1', '20');
INSERT INTO `mssg`.`organization` (`id`, `organizationName`, `phoneNumber`, `active`) VALUES ('1', 'MillionSpaces', '0111234567', '1');
INSERT INTO `mssg`.`user` (`id`, `userName`, `email`, `password`, `phoneNumber`, `userRoleId`, `addressId`, `verified`) VALUES ('1', 'priyan', 'priyan@ms.com', '123456', '0776475748', '1', '1', '1');
INSERT INTO `mssg`.`space` (`id`, `spaceName`, `description`, `organizationId`, `userId`, `addressId`, `commissionId`, `active`, `delete`) VALUES ('1', 'MeetingRoom-1', 'million spaces meeting room 1', '1', '1', '1', '1', '1', '0');
