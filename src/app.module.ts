import { Module, Logger, CacheModule, CacheInterceptor } from '@nestjs/common';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { AppController } from './controller/app.controller';
import { AppService } from './service/app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './repository/user.repository';
import { UserController } from './controller/user.controller';
import { UserService } from './service/user.service';
import { SpaceController } from './controller/space.controller';
import { SpaceService } from './service/space.service';
import { SpaceRepository } from './repository/space.repository';
import { RolesGuard } from './common/guards/roles.guard';
import * as memcachedStore from 'cache-manager-memcached-store';
import * as config from 'config';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersResolvers } from './resolver/users.resolvers';
import { SpacesResolvers } from './resolver/spaces.resolvers';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: config.get('dbConfig.dbUrl'),
      port: 3306,
      username: config.get('dbConfig.dbUserName'),
      password: config.get('dbConfig.dbPassword'),
      database: config.get('dbConfig.dbName'),
      connectTimeout: 60 * 60 * 1000,
      acquireTimeout: 60 * 60 * 1000,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
      logging: false,
    }),
    TypeOrmModule.forFeature([UserRepository, SpaceRepository]),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
    }),
  ],
  controllers: [AppController, UserController, SpaceController],
  providers: [
    AppService,
    UserService,
    Logger,
    SpaceService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
    UsersResolvers,
    SpacesResolvers,
  ],
})
export class AppModule {}
