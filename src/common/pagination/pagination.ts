export class Pagination<PaginationEntity> {
  public results: PaginationEntity[];
  public pageTotal: number;
  public total: number;

  constructor(results: PaginationEntity[], total: number) {
    this.results = results;
    this.pageTotal = results.length;
    this.total = total;
  }
}
