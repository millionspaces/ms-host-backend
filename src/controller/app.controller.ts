import {
  Get,
  Controller,
  Logger,
  UseInterceptors,
  CacheInterceptor,
} from '@nestjs/common';
import { AppService } from '../service/app.service';
import {
  ApiResponse,
  ApiUseTags,
  ApiOperation,
  ApiExcludeEndpoint,
  ApiOkResponse,
  ApiProduces,
} from '@nestjs/swagger';

@Controller()
@ApiUseTags('ms')
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger,
  ) {}

  @Get()
  @ApiOperation({ title: 'Welcome', description: 'Rest API for welcome' })
  @ApiResponse({ status: 200, description: 'Return all Spaces.', type: String })
  welcome(): string {
    this.logger.log('AppController:welcome called');
    return this.appService.welcome();
  }
}
