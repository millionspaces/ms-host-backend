import {
  Get,
  Controller,
  Logger,
  UseGuards,
  ReflectMetadata,
  Param,
  Post,
  Body,
} from '@nestjs/common';
import {
  ApiResponse,
  ApiUseTags,
  ApiOperation,
  ApiImplicitParam,
} from '@nestjs/swagger';
import { SpaceService } from '../service/space.service';
import { Space } from '../model/space.entity';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../common/decorators/roles.decorator';

@Controller('space')
@ApiUseTags('ms')
@UseGuards(RolesGuard)
export class SpaceController {
  constructor(
    private readonly spaceService: SpaceService,
    private readonly logger: Logger,
  ) {}

  @Get('page/:page')
  @Roles('admin')
  @ApiOperation({
    title: 'Get All Pagination Spaces ',
    description: 'Rest API for getting all spaces with pagination from 0 to n',
  })
  @ApiResponse({
    status: 200,
    description: 'Return all Spaces.',
    type: Array.of(Space),
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({
    name: 'page',
    description: 'number of page : 0...n',
    required: true,
  })
  async getAllSpaces(@Param('page') page: number): Promise<any> {
    this.logger.log('SpaceController:getAllSpaces called');
    return this.spaceService.getAllSpaces(+page);
  }

  @Get('profile/:spaceID')
  @Roles('admin')
  @ApiOperation({
    title: 'Get Space Info',
    description: 'Rest API for getting details about the Space',
  })
  @ApiResponse({
    status: 200,
    description: 'Return Space Details.',
    type: Space,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({
    name: 'Space',
    description: 'Space Details',
    required: true,
  })
  async getSpace(@Param('spaceID') space: number): Promise<any> {
    this.logger.log('SpaceController:getSpace called');
    return this.spaceService.getSpace(space);
  }

  @Get('delete/:spaceID')
  @Roles('admin')
  @ApiOperation({
    title: 'Unapprove the Space',
    description: 'Rest API for Delisting the Space',
  })
  @ApiResponse({
    status: 200,
    description: 'Sucess',
    type: Space,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({
    name: 'Space',
    description: 'Space Details',
    required: true,
  })
  async deactivateSpace(@Param('spaceID') space: number): Promise<any> {
    this.logger.log('SpaceController:deactivateSpace called');
    return this.spaceService.deactivateSpace(space);
  }

  @Post('create')
  @Roles('admin')
  @ApiOperation({
    title: 'Create a new Space',
    description: 'Rest API for creating a Space',
  })
  @ApiResponse({
    status: 200,
    description: 'Sucess',
    type: Space,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({
    name: 'Space',
    description: 'Space Details',
    required: true,
  })
  async createSpace(@Body() space: Space): Promise<any> {
    this.logger.log('SpaceController:createSpace called');
    return this.spaceService.createNewSpace(space);
  }

  @Post('update')
  @Roles('admin')
  @ApiOperation({
    title: 'Update the Space',
    description: 'Rest API for Updating the Space',
  })
  @ApiResponse({
    status: 200,
    description: 'Sucess',
    type: Space,
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({
    name: 'Space',
    description: 'Space Details',
    required: true,
  })
  async updateSpace(@Body() space: Space): Promise<any> {
    this.logger.log('SpaceController:updateSpace called');
    return this.spaceService.updateSpace(space);
  }
}
