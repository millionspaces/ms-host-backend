import { Controller, Get, Logger, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Space } from '../model/space.entity';
import { User } from '../model/user.entity';
import { UserService } from '../service/user.service';

@Controller('user')
@ApiUseTags('ms')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly logger: Logger,
  ) {}

  @Get('all')
  @ApiOperation({
    title: 'Get All Users',
    description: "Rest API for getting all user's",
  })
  @ApiResponse({
    status: 200,
    description: 'Return all Users.',
    type: Array.of(User),
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  public async getAllUsers(): Promise<User[]> {
    this.logger.log('UserController:getAllUsers called');
    return this.userService.getAllUsers();
  }

  @Get(':userID/spaces')
  @ApiOperation({
    title: 'Get All Spaces of a User',
    description: 'Rest API for getting the Spaces of a User',
  })
  @ApiResponse({
    status: 200,
    description: 'Return all Users.',
    type: Array.of(User),
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  public async getAllSpaces(@Param('userID') user: number): Promise<Space[]> {
    this.logger.log('UserController:getAllSpaces called');
    return this.userService.getAllSpacesofUser(user);
  }
}
