import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { RolesGuard } from './common/guards/roles.guard';
import * as config from 'config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {});
  app.setGlobalPrefix(config.get('basePath'));

  const options = new DocumentBuilder()
    .setTitle('Ms Host Backend')
    .setDescription('The ms host backend rest api')
    .setSchemes('http', 'https')
    .setVersion('1.0')
    .setBasePath(config.get('basePath'))
    .addTag('ms')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  const port = process.env.PORT || 3000;
  await app.listen(port, '0.0.0.0', () => {
    // console.log(`${config.get('env')} Listening on Port 3000`);
  });
}
bootstrap();
