import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Space } from './space.entity';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

@Entity()
export class Address {
  @PrimaryGeneratedColumn()
  @ApiModelProperty({
    description: 'address unique id',
    required: true,
    type: 'number',
    example: 1,
  })
  id: number;

  @Column()
  @ApiModelProperty({
    description: 'address line 1',
    required: true,
    type: 'string',
    example: '61 Janadhipathi Mawatha',
  })
  line1: string;

  @Column()
  @ApiModelPropertyOptional({
    description: 'address line 1',
    type: 'string',
    example: 'Colombo 1',
  })
  line2: string;

  @Column()
  @ApiModelProperty({
    description: 'zip code',
    required: true,
    type: 'string',
    example: '00300',
  })
  zipCode: string;

  @Column()
  @ApiModelProperty({
    description: 'city',
    required: true,
    type: 'string',
    example: 'Colombo',
  })
  city: string;

  @Column()
  @ApiModelProperty({
    description: 'latitude',
    required: true,
    type: 'string',
    example: '6.927079',
  })
  latitude: string;

  @Column()
  @ApiModelProperty({
    description: 'longitude',
    required: true,
    type: 'string',
    example: '79.861244',
  })
  longitude: string;

  @OneToMany(type => User, user => user.address)
  users: User[];

  @OneToMany(type => Space, space => space.address)
  spaces: Space[];
}
