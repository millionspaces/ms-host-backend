import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Space } from './space.entity';

@Entity()
export class Commission {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  commission: number;

  @OneToMany(type => Space, space => space.commission)
  spaces: Space[];
}
