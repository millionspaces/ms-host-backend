import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Space } from './space.entity';

@Entity()
export class Organization {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  organizationName: string;

  @Column()
  phoneNumber: string;

  @OneToMany(type => Space, space => space.organization)
  spaces: Space[];

  @Column('int')
  active: boolean;
}
