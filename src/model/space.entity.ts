import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Organization } from './organization.entity';
import { Commission } from './commission.entity';
import { Address } from './address.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class Space {
  @PrimaryGeneratedColumn()
  @ApiModelProperty({
    description: 'space unique id',
    required: true,
    type: 'number',
    example: 1,
  })
  id: number;

  @Column()
  @ApiModelProperty({
    description: 'space name',
    required: true,
    type: 'string',
    example: 'Metting Room',
  })
  spaceName: string;

  @Column()
  @ApiModelProperty({
    description: 'description about space',
    required: true,
    type: 'string',
    example:
      'A quiet place in the close environs of the World Trade Centre and Dutch Hospital',
  })
  description: string;

  @ManyToOne(type => User, user => user.spaces, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'space owner',
    required: true,
    type: 'User',
    example: {
      id: 1,
      userName: 'priyan',
      email: 'priyan@ms.com',
      phoneNumber: '0776475748',
    },
  })
  user: User;

  @ManyToOne(type => Organization, organization => organization.spaces, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'space organization',
    required: true,
    type: 'Organization',
    example: {
      id: 1,
      organizationName: 'MillionSpaces',
      phoneNumber: '0111234567',
    },
  })
  organization: Organization;

  @ManyToOne(type => Commission, commission => commission.spaces, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'space commission',
    required: true,
    type: 'Commission',
    example: { id: 1, commission: 20 },
  })
  commission: Commission;

  @ManyToOne(type => Address, address => address.spaces, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'address of space',
    required: true,
    type: 'Address',
    example: {
      id: 1,
      line1: '61 Janadhipathi Mawatha',
      line2: 'Colombo 1',
      zipCode: '00300',
      city: 'Colombo',
      latitude: '6.927079',
      longitude: '79.861244',
    },
  })
  address: Address;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  @ApiModelProperty({
    description: 'space created date',
    required: true,
    type: 'Date',
    example: '2019-03-04T10:49:26.000Z',
  })
  created: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  @ApiModelProperty({
    description: 'space updated date',
    required: true,
    type: 'Date',
    example: '2019-03-04T10:49:26.000Z',
  })
  updated: Date;

  @Column('int')
  active: boolean;

  @Column('int')
  delete: boolean;
}
