import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { UserRole } from './user.role.entity';
import { Address } from './address.entity';
import { Space } from './space.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  @ApiModelProperty({
    description: 'user unique id',
    required: true,
    type: 'number',
    example: 1,
  })
  id: number;

  @Column()
  @ApiModelProperty({
    description: 'user name',
    required: true,
    type: 'string',
    example: 'user1',
  })
  userName: string;

  @Column()
  @ApiModelProperty({
    description: 'user email',
    required: true,
    type: 'string',
    example: 'user@ms.com',
  })
  email: string;

  @Column()
  @ApiModelProperty({
    description: 'user phone number',
    required: true,
    type: 'string',
    example: '0771234567',
  })
  phoneNumber: string;

  @ManyToOne(type => UserRole, user_role => user_role.users, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'user role',
    required: true,
    type: 'UserRole',
    example: { id: 1, name: 'USER' },
  })
  userRole: UserRole;

  @ManyToOne(type => Address, address => address.users, {
    eager: true,
  })
  @ApiModelProperty({
    description: 'address of user',
    required: true,
    type: 'Address',
    example: {
      id: 1,
      line1: '61 Janadhipathi Mawatha',
      line2: 'Colombo 1',
      zipCode: '00300',
      city: 'Colombo',
      latitude: '6.927079',
      longitude: '79.861244',
    },
  })
  address: Address;

  @OneToMany(type => Space, space => space.user)
  spaces: Space[];

  @Column('int')
  verified: boolean;
}
