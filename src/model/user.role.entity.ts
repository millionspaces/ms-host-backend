import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class UserRole {
  @PrimaryGeneratedColumn()
  @ApiModelProperty({
    description: 'user role unique id',
    required: true,
    type: 'number',
    example: 1,
  })
  id: number;

  @Column()
  @ApiModelProperty({
    description: 'user role name',
    required: true,
    type: 'string',
    example: 'USER',
    enum: ['USER', 'ADMIN', 'GUEST'],
  })
  name: string;

  @OneToMany(type => User, user => user.userRole)
  users: User[];

  @Column('int')
  active: boolean;
}
