import { Repository, EntityRepository } from 'typeorm';
import { Space } from '../model/space.entity';

@EntityRepository(Space)
export class SpaceRepository extends Repository<Space> {}
