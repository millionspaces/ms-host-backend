import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { ParseIntPipe } from '@nestjs/common';
import { UserService } from '../service/user.service';
import { User } from '../model/user.entity';
import { SpaceService } from '../service/space.service';

@Resolver('Space')
export class SpacesResolvers {
  constructor(private readonly spaceService: SpaceService) {}

  @Query()
  async getSpaces(@Args('page') page: number) {
    return this.spaceService.getAllSpaces(page);
  }
}
