import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { ParseIntPipe } from '@nestjs/common';
import { UserService } from '../service/user.service';
import { User } from '../model/user.entity';

@Resolver('User')
export class UsersResolvers {
  constructor(private readonly userService: UserService) {}

  @Query()
  async getUsers() {
    return this.userService.getAllUsers();
  }
}
