import { Injectable, Logger } from '@nestjs/common';
import { SpaceRepository } from '../repository/space.repository';
import { UserRepository } from '../repository/user.repository';
import { Space } from '../model/space.entity';
import { Pagination } from '../common/pagination/pagination';
import { UpdateResult } from 'typeorm';

@Injectable()
export class SpaceService {
  constructor(
    private readonly spaceRepository: SpaceRepository,
    private readonly userRepository: UserRepository,
    private readonly logger: Logger,
  ) {}

  async createNewSpace(space: Space) {
    this.logger.log('SpaceService:createNewSpace called');
    space.id = (await this.spaceRepository.count()) + 1;
    const tempUser = await this.userRepository.findOne({ id: space.user.id });
    if (tempUser) {
      space.user = tempUser;
      const result = await this.spaceRepository.insert(space);
      return result;
    }
    return 'Invalid user';
  }

  async getAllSpaces(page: number): Promise<Pagination<Space>> {
    this.logger.log('SpaceService:getAllSpaces called ');
    const [results, total] = await this.spaceRepository.findAndCount({
      take: 2,
      skip: page * 2,
    });
    return new Pagination<Space>(results, total);
  }

  async getSpace(spaceID: number): Promise<Space> {
    this.logger.log('SpaceService:getSpace called');
    const result = await this.spaceRepository.findOneOrFail({ id: spaceID });
    return result;
  }

  async updateSpace(space: Space): Promise<UpdateResult> {
    this.logger.log('SpaceService:updateSpace called');
    const result = await this.spaceRepository.update({ id: space.id }, space);
    return result;
  }

  // Set Methods
  async deactivateSpace(spaceID: number) {
    this.logger.log('SpaceService:deactivateSpace called');
    const update = await this.spaceRepository.update(
      { id: spaceID },
      { active: false },
    );
  }

  async setSpaceName(spaceID: number, newName: string) {
    this.logger.log('SpaceService:setSpaceName called');
    const update = await this.spaceRepository.update(
      { id: spaceID },
      { spaceName: newName },
    );
  }
}
