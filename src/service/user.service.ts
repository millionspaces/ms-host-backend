import { Injectable, Logger } from '@nestjs/common';
import { UserRepository } from '../repository/user.repository';
import { SpaceRepository } from '../repository/space.repository';
import { User } from '../model/user.entity';
import { Space } from '../model/space.entity';

@Injectable()
export class UserService {
  constructor(
    private readonly spaceRepository: SpaceRepository,
    private readonly userRepository: UserRepository,
    private readonly logger: Logger,
  ) {}
  async getAllUsers(): Promise<User[]> {
    this.logger.log('UserService:getAllUsers called');
    return this.userRepository.find();
  }

  async getAllSpacesofUser(userID: number): Promise<Space[]> {
    this.logger.log('UserService:getAllSpacesofUser called');
    return this.spaceRepository.find({ user: { id: userID } });
  }
}
